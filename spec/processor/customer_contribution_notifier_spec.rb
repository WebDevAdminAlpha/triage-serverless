# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/customer_contribution_notifier'
require_relative '../../triage/triage/event'

RSpec.describe Triage::CustomerContributionNotifier do
  subject { described_class.new(event, messenger: messenger_stub) }

  let(:event_author) { Triage::Event::ENG_PROD_TEAM_USERNAMES.first }
  let(:contribution_type) { 'bug' }
  let(:url) { 'http://gitlab.com/mr_url' }
  let(:org_name) { 'org' }
  let(:event) do
    instance_double('Triage::MergeRequestEvent',
      from_gitlab_org?: true,
      wider_community_author?: true,
      merge_request?: true,
      merge_event?: true,
      author_id: 42,
      label_names: [contribution_type],
      url: url)
  end

  let(:messenger_stub) { double }

  before do
    allow(Triage::OrgByUsernameLocator).to receive(:locate_org).and_return(org_name)
    allow(messenger_stub).to receive(:ping)
  end

  describe '#process' do
    shared_examples 'no message posting' do
      it 'does not call #notify_customer_contribution_channel' do
        expect(subject).not_to receive(:notify_customer_contribution_channel)

        subject.process
      end
    end

    shared_examples 'message posting' do
      it 'posts a customer contribution message' do
        body = <<~MARKDOWN
          > Organization: #{described_class::CUSTOMER_PORTAL_URL}#{org_name}
          > Contribution Type: #{contribution_type}
          > MR Link: #{url}
        MARKDOWN

        subject.process
        expect(messenger_stub).to have_received(:ping).exactly(1).times.with(body)
      end
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not for a merge request' do
      before do
        allow(event).to receive(:merge_request?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not for a merged mr' do
      before do
        allow(event).to receive(:merge_event?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not from a wider community member' do
      before do
        allow(event).to receive(:wider_community_author?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not associated with an org' do
      before do
        allow(Triage::OrgByUsernameLocator).to receive(:locate_org).and_return(nil)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is for a new merge request opened by a wider community author with org under the gitlab-org group' do
      it_behaves_like 'message posting'
    end
  end
end
