# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/thank_community_contribution'
require_relative '../../triage/triage/event'

RSpec.describe Triage::ThankCommunityContribution do
  subject { described_class.new(event) }

  let(:event_author) { Triage::Event::ENG_PROD_TEAM_USERNAMES.first }
  let(:project_id) { 278964 }
  let(:event) do
    instance_double('Triage::Event',
      from_gitlab_org?: true,
      wider_community_author?: true,
      merge_request?: true,
      new_entity?: true,
      user_username: event_author,
      noteable_path: '/foo',
      project_id: project_id)
  end

  describe '#applicable?' do
    context 'when event is for a new merge request opened by a wider community author under the gitlab-org group' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not for a merge request' do
      before do
        allow(event).to receive(:merge_request?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not for a new entity' do
      before do
        allow(event).to receive(:new_entity?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from a wider community author' do
      before do
        allow(event).to receive(:wider_community_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'for project with custom conditions' do
      let(:project_id) { described_class::WWW_GITLAB_COM_PROJECT_ID }

      before do
        allow(event).to receive(:from_gitlab_com?).and_return(false)
      end

      it 'processes different conditions' do
        expect(event).to receive(:from_gitlab_com?)
        subject.applicable?
      end
    end
  end

  describe '#process' do
    let(:expected_message_template) { Triage::Strings::Thanks::DEFAULT_THANKS }
    let(:expected_message) { format(expected_message_template, author_username: event_author) }

    it 'posts a default message' do
      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end

    context 'when project_id has override thanks' do
      let(:expected_message_template) { described_class::PROJECT_THANKS[project_id][:message] }

      context 'runner' do
        let(:project_id) { 250833 }

        it 'posts custom project comment' do
          expect_comment_request(event: event, body: expected_message) do
            subject.process
          end
        end
      end

      context 'website' do
        let(:project_id) { 7764 }

        it 'posts custom project comment' do
          expect_comment_request(event: event, body: expected_message) do
            subject.process
          end
        end
      end
    end
  end
end
