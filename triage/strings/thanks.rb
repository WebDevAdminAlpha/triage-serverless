# frozen_string_literal: true

module Triage
  module Strings
    module Thanks
      DEFAULT_THANKS = <<~MARKDOWN.chomp
        :wave: @%<author_username>s

        Thank you for your contributions to GitLab. We believe that [everyone can contribute](https://about.gitlab.com/company/strategy/#everyone-can-contribute) and contributions like yours are what make GitLab great!

        Our [Merge Request coaches](https://about.gitlab.com/company/team/?department=merge-request-coach) will ensure your contribution is reviewed in a timely manner.
        To learn more about the merge request triage process you can refer to the [Wider Community Merge Request Triage page](https://about.gitlab.com/handbook/engineering/quality/merge-request-triage).

        To bring your merge request to the attention of the relevant team within GitLab, you can ask our bot to label it with a [group label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#group-labels).
        For example, if your merge request changes a project management feature, it can be labelled by commenting `@gitlab-bot label ~"group::project management"`.
        To find the most relevant group for your change, you can look up the group based on the most relevant product category in [the product categories table](https://about.gitlab.com/handbook/product/categories/#categories-a-z).
        Once you have found the group name, type `@gitlab-bot label ~group::`, then start to type the group name and select the applicable group label, then submit the comment and the bot will apply the label for you.

        If after a few days, there's no message from a GitLab team member, feel free to ping `@gitlab-org/coaches` or ask for help by commenting `@gitlab-bot help`.

        These resources may help you to move your Merge Request to the next steps:

        - [Contributing to GitLab](https://about.gitlab.com/community/contribute/)
        - [How to get help](https://about.gitlab.com/community/contribute/#getting-help)
        - [Ping a Merge request coach](https://about.gitlab.com/company/team/?department=merge-request-coach)

        *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#ensure-quick-feedback-for-community-contributions). You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-serverless/-/blob/master/triage/processor/thank_community_contribution.rb).*

        /label ~"Community contribution"
      MARKDOWN

      RUNNER_THANKS = <<~MARKDOWN.chomp
          Thank you for your contribution! :tada:

          We strive to make the contribution experience as smooth as possible.

          Some contributions require several iterations of review and we try to mentor contributors during this process. However, we understand that some reviews can be very time consuming. If you would prefer us to continue the work you've submitted now or at any point in the future please let us know.

          If you're okay with being part of our review process (and we hope you are!), there are several initial checks we ask you to make:

          * The merge request description clearly explains:
            * The problem being solved.
            * The best way a reviewer can test your changes (is it possible to provide an example?).
          * If the pipeline failed, do you need help identifying what failed?
          * Check that Go code follows our [Go guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
          * Read our [contributing to GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/CONTRIBUTING.md#contribute-to-gitlab-runner) document.

          ---

          We sometimes get a large number of community contributions and have to prioritize reviews.

          *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#ensure-quick-feedback-for-community-contributions). You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-serverless/-/blob/master/triage/strings/thanks.rb).*
        MARKDOWN

      WWW_GITLAB_COM_THANKS = <<~MARKDOWN.chomp
         Hi @%<author_username>s,

         Thanks so much for your contribution to the GitLab website! :heart:

         I'll notify the Website team about your Merge Request and they will get back to you as soon as they can.
         If you don't hear from someone in a reasonable amount of time, please ping us again in a comment and mention @gl-website.

         /label ~"Community contribution"
        MARKDOWN
    end
  end
end
