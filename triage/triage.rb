# frozen_string_literal: true

require 'mini_cache'
require 'gitlab'
require 'sucker_punch'

require_relative 'triage/error'
require_relative 'triage/event'
require_relative 'triage/handler'
require_relative 'triage/sentry'

module Triage
  PRODUCTION_API_ENDPOINT = 'https://gitlab.com/api/v4'
  GITLAB_ORG_GROUP = 'gitlab-org'
  GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION = 3600 # 1 hour
  GITLAB_COM_GROUP = 'gitlab-com'
  GITLAB_COM_GROUP_MEMBERS_CACHE_EXPIRATION = 3600 # 1 hour
  GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP = 'gitlab-org/gitlab-core-team/community-members'
  GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP_MEMBERS_CACHE_EXPIRATION = 3600 # 1 hour

  def self.run!(context:, event:)
    handler = Handler.new(context, event)

    puts "Running in dry mode" if dry_run?

    if handler.authenticated?
      messages = handler.process

      puts "Incoming webhook event: #{event.inspect}, messages: #{messages.inspect}"

      { status: :ok, messages: messages }
    else
      puts "Unauthenticated request!"

      { status: :unauthenticated }
    end
  rescue Triage::ClientError => error # Don't track client errors
    error_response(error)
  rescue => error
    Raven.tags_context(object_kind: event['object_kind'])
    Raven.extra_context(event: event)
    Raven.capture_exception(error)

    puts "Exception with event: #{event.inspect}, error: #{error.class}: #{error.message}"
    error_response(error)
  end

  def self.error_response(error)
    { status: :error, error: error.class, message: error.message }
  end

  def self.dry_run?
    !ENV['DRY_RUN'].nil?
  end

  def self.cache
    @cache ||= MiniCache::Store.new
  end

  def self.api_client
    cache.get_or_set(:api_client) do
      Gitlab.client(endpoint: PRODUCTION_API_ENDPOINT, private_token: ENV['GITLAB_API_TOKEN'].to_s)
    end
  end

  def self.gitlab_org_group_member_usernames(fresh: false)
    group_member_usernames(GITLAB_ORG_GROUP, fresh: fresh, expires_in: GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  def self.gitlab_com_group_member_usernames(fresh: false)
    group_member_usernames(GITLAB_COM_GROUP, fresh: fresh, expires_in: GITLAB_COM_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  def self.gitlab_core_team_community_members_group_member_usernames(fresh: false)
    group_member_usernames(GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP, fresh: fresh, expires_in: GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  def self.group_member_usernames(group, fresh:, expires_in:)
    cache_method = fresh ? :set : :get_or_set

    cache.public_send(cache_method, "#{group}_group_member_usernames".to_sym) do
      group_members = fresh_group_member_usernames(group)
      MiniCache::Data.new(group_members, expires_in: expires_in)
    end
  end
  private_class_method :group_member_usernames

  def self.fresh_group_member_usernames(group)
    api_client
      .group_members(group, per_page: 100)
      .auto_paginate
      .map(&:username)
  end
  private_class_method :fresh_group_member_usernames
end
